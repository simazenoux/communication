# Communication
Mes TP de communication de première année à l'ISIMA

## TP1 : Résumé
L'objectif de ce TP était d'effectuer le résumé d'un article de presse en 150-200 mots.

## TP2 : Revue de presse
L'objectif était de sélectionner 3 documents de presse, puis de les analyser ensemble lors d'une présentation orale de 15 minutes en binome.

## TP3 : Exposé de cours
En binome, réaliser un diaporama correspondant à une partie des documents du polycopié de communication, avec des exemples personnels.

## TP4 : Exposé individuel
Faire un exposé oral avec un diaporama d'une durée de 15 minutes sur un thème prédéfini (ici égalité hommes femmes)
